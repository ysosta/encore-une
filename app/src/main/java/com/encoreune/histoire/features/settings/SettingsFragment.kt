package com.encoreune.histoire.features.settings

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ShareCompat
import android.support.v4.content.ContextCompat
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.util.Log
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.encoreune.histoire.BuildConfig
import com.encoreune.histoire.MyApplication
import com.encoreune.histoire.R
import com.encoreune.histoire.data.PurchasePojo
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*


class SettingsFragment : PreferenceFragmentCompat() {

    private var mFirebaseAnalytics: FirebaseAnalytics? = null


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.advanced_preferences, rootKey)

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity!!.applicationContext)


//        val toggleNotif = findPreference("notifications_checked")
//        toggleNotif.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
//            OneSignal.setSubscription(newValue as Boolean)
//            true
//        }
//
//        val toggleSound = findPreference("notifications_sound")
//        toggleSound.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
//            OneSignal.enableSound(newValue as Boolean)
//            true
//        }
//
//        val toggleVibrate = findPreference("notifications_vibration")
//        toggleVibrate.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
//            OneSignal.enableVibrate(newValue as Boolean)
//            true
//        }

        //Compte
        val preferenceUser = findPreference("user")

        val preferenceSubscrib = findPreference("subscription")

        val user = FirebaseAuth.getInstance().currentUser
        if (user == null) preferenceUser.summary = "Aucun"
        else {
            preferenceUser.summary = user.displayName

            val database = MyApplication.database
            val myRef = database.getReference("purchases/"+user.uid)

            // Read from the database and retrieve stories for the language defined
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val purchase = dataSnapshot.getValue(PurchasePojo::class.java)

                    if (purchase != null && purchase.isActive!!) {
                        val date = Date(purchase.expiryTimeMillis!!.toLong())
                        val dt1 = SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH)
                        preferenceSubscrib.summary = "Jusqu'au " + dt1.format(date)
                        preferenceSubscrib.title = "Abonnement mensuel en cours"

                        preferenceSubscrib.setOnPreferenceClickListener {preference: Preference? ->
                            val browserIntent = Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/account/subscriptions"))
                            startActivity(browserIntent)

                            true
                        }
                    }else {
                        preferenceSubscrib.title = "Abonnement"
                        preferenceSubscrib.summary = "Aucun abonnement actuellement en cours."
                    }

                }

                override fun onCancelled(databaseError: DatabaseError) {

                    // Failed to read value
                    Log.d("databaseError", databaseError.toException().toString())

                    preferenceSubscrib.title = "Abonnement"
                    preferenceSubscrib.summary = "Aucun abonnement actuellement en cours."
                }
            })

        }

        // Noter l'application sur le Store
        val preferencerate = findPreference("rate")
        preferencerate.onPreferenceClickListener = Preference.OnPreferenceClickListener {



            val bundle = Bundle()
            mFirebaseAnalytics!!.logEvent("RATE_THE_APP", bundle)


            val uri = Uri.parse("market://details?id=" + activity!!.packageName)
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            try {
                startActivity(goToMarket)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(activity,
                        "Could not open Play Store",
                        Toast.LENGTH_SHORT).show()
                return@OnPreferenceClickListener true
            }

            true
        }


        val preferencemail = findPreference("mail")
        preferencemail.onPreferenceClickListener = Preference.OnPreferenceClickListener {


            val bundle = Bundle()
            mFirebaseAnalytics!!.logEvent("CONTACT_US", bundle)

            val builder = ShareCompat.IntentBuilder.from(activity)
            builder.setType("message/rfc822")
            builder.addEmailTo("encoreunehistoire@ideastive.com")
            builder.setSubject(getString(R.string.app_name) + " | App version : " + BuildConfig.VERSION_NAME)
            builder.startChooser()

            true
        }


        val preferenceabout = findPreference("about")
        preferenceabout.onPreferenceClickListener = Preference.OnPreferenceClickListener {

            val builder = MaterialDialog.Builder(activity!!)
                    .title(R.string.toolbar_app_name)
                    .icon(ContextCompat.getDrawable(activity!!, R.mipmap.ic_launcher)!!)
                    .content(R.string.about_content)
                    .positiveText(R.string.agree)

            val dialog = builder.build()
            dialog.show()

            true
        }


        val preferencethanks_to = findPreference("thanks_to")
        preferencethanks_to.onPreferenceClickListener = Preference.OnPreferenceClickListener {

            val builder = MaterialDialog.Builder(activity!!)
                    .title(R.string.thanks_to)
                    .icon(ContextCompat.getDrawable(activity!!, R.mipmap.ic_launcher)!!)
                    .content(R.string.thanks_to_content)
                    .positiveText(R.string.thanks_to_you)

            val dialog = builder.build()
            dialog.show()

            true
        }
    }




}