package com.encoreune.histoire.features.settings

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.encoreune.histoire.R
import kotlinx.android.synthetic.main.activity_settings.*


class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        toolbar_title.text = getString(R.string.settings)
        val face = Typeface.createFromAsset(assets, "fonts/Royana-Regular.ttf")
        toolbar_title.typeface = face
        setSupportActionBar(toolbar)

        toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
    }

    override fun onResume() {
        super.onResume()

        val frag : Fragment = SettingsFragment()

        supportFragmentManager.beginTransaction().replace(R.id.content_frame, frag).commit()
    }
}
