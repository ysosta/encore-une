package com.encoreune.histoire.features.stories.list

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.android.billingclient.api.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.encoreune.histoire.MyApplication
import com.encoreune.histoire.R
import com.encoreune.histoire.data.BookPojo
import com.encoreune.histoire.data.PurchasePojo
import com.encoreune.histoire.features.stories.adapters.StoriesAdapter
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.activity_cover.*
import java.util.*

class CoverStoriesActivity : AppCompatActivity(), PurchasesUpdatedListener, BillingClientStateListener {

    private var book: BookPojo? = null
    private val RC_SIGN_IN = 123
    private lateinit var adapter: StoriesAdapter
    private lateinit var billingClient: BillingClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cover)

        supportPostponeEnterTransition()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }

        billingClient = BillingClient.newBuilder(this).setListener(this).build()



        val b = this.intent.extras
        if (b != null) {
            book = b.getParcelable("book")
        }

    }

    override fun onResume() {
        super.onResume()
        billingClient.startConnection(this)
    }

    override fun onBillingServiceDisconnected() {
        Log.e("Billing setup", "Service disconnected")
    }

    override fun onBillingSetupFinished(responseCode: Int) {
        loadData()
    }

    fun purchaseSubscription(){
        val user = FirebaseAuth.getInstance().getCurrentUser()
        if (user!=null) {
            val flowParams = BillingFlowParams.newBuilder()
                    .setSku("monthly")
                    .setType(BillingClient.SkuType.SUBS)
                    .build()
            billingClient.launchBillingFlow(this, flowParams)

        }else{

            val providers = Arrays.asList(
                    AuthUI.IdpConfig.GoogleBuilder().build())

            // Create and launch sign-in intent
            startActivityForResult(
            AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build(), RC_SIGN_IN)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val flowParams = BillingFlowParams.newBuilder()
                        .setSku("monthly")
                        .setType(BillingClient.SkuType.SUBS)
                        .build()
                billingClient.launchBillingFlow(this, flowParams)
            } else {
                Toast.makeText(this,"Connexion echouee",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun loadData(){

        val database = MyApplication.database
        val user = FirebaseAuth.getInstance().currentUser
        val myRef = database.getReference("purchases/"+user!!.uid)

        // Read from the database and retrieve stories for the language defined
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var is_active = false
                val purchase = dataSnapshot.getValue(PurchasePojo::class.java)

                if (purchase != null && purchase.isActive!!) is_active = true


                val requestBuilder =
                        Glide.with(this@CoverStoriesActivity)
                                .load(book!!.image)


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    val listener = (object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            this@CoverStoriesActivity.supportStartPostponedEnterTransition()
                            showList()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            this@CoverStoriesActivity.supportStartPostponedEnterTransition()
                            showList()
                            return false
                        }


                    })

                    requestBuilder.listener(listener)
                }else showList()

                requestBuilder
                        .into(kenburnsimageView)


            }

            override fun onCancelled(databaseError: DatabaseError) {

                // Failed to read value
                Log.d("databaseError", databaseError.toException().toString())

                val requestBuilder =
                        Glide.with(this@CoverStoriesActivity)
                                .load(book!!.image)


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    val listener = (object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            this@CoverStoriesActivity.supportStartPostponedEnterTransition()
                            showList()
                            return true
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            this@CoverStoriesActivity.supportStartPostponedEnterTransition()
                            showList()
                            return true
                        }


                    })

                    requestBuilder.listener(listener)
                }else showList()

                requestBuilder
                        .into(kenburnsimageView)



            }
        })


    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        if ( purchases != null && purchases.isNotEmpty()){
            Log.d("purchases",purchases.toString())
            
            val user = FirebaseAuth.getInstance().currentUser

            for (purchase in purchases){

                val mServiceReference = MyApplication.database.getReference("purchases/"+user!!.uid)

                mServiceReference.runTransaction(object : Transaction.Handler {
                    override fun onComplete(databaseError: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                        Log.d("Transactioncompleted", "postTransaction:onComplete " +databaseError.toString())
                    }

                    override fun doTransaction(mutableData: MutableData): Transaction.Result {

                            val purchasePojo = PurchasePojo()
                            purchasePojo.orderId = purchase.orderId
                            purchasePojo.productId = purchase.sku
                            purchasePojo.packageName = purchase.packageName
                            purchasePojo.purchaseTime = purchase.purchaseTime.toString()
                            purchasePojo.purchaseToken = purchase.purchaseToken
                            purchasePojo.autoRenewing = purchase.isAutoRenewing
                            purchasePojo.expiryTimeMillis = 0.toString()
                            purchasePojo.isActive = true

                            mutableData.setValue(purchasePojo)
                            return Transaction.success(mutableData)
                    }
                })

            }
        }
    }

    private fun showList(){
        val tface = Typeface.createFromAsset(this@CoverStoriesActivity.assets, "fonts/Royana-Regular.ttf")
        title_current_book.setTextColor(Color.parseColor(book!!.title_color))
        title_current_book.text = book!!.title
        title_current_book.typeface=tface

        list_of_stories_recyclerView.addOnItemChangedListener { _, adapterPosition ->

            title_current_story.typeface=tface
            val conte = adapter.getItem(adapterPosition)
            title_current_story.text = conte.title
            title_current_story.text = conte.title
            title_current_story.setTextColor(Color.parseColor(conte.title_color))
            bottom_story_view.setBackgroundColor(Color.parseColor(conte.vibrant_color))
        }

        adapter = StoriesAdapter(this@CoverStoriesActivity, book!!.stories!!, false)
        list_of_stories_recyclerView.setAdapter(adapter)
        list_of_stories_recyclerView.setItemTransitionTimeMillis(150)
        list_of_stories_recyclerView.setItemTransformer(ScaleTransformer.Builder()
                .setMinScale(0.85f)
                .build())
    }
    override fun onBackPressed() {
        supportFinishAfterTransition()
        super.onBackPressed()
    }
}
