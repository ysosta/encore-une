package com.encoreune.histoire.features.stories.read

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import com.encoreune.histoire.R
import com.encoreune.histoire.data.ContePojo
import kotlinx.android.synthetic.main.activity_story_reader.*
import me.kaelaela.verticalviewpager.transforms.DefaultTransformer


class StoryReaderActivity : AppCompatActivity() {


    private var conte: ContePojo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_reader)

        val b = this.intent.extras
        if (b != null) {
            conte = b.getParcelable("story")
        }

        val adapterViewPager = MyPagerAdapter(getSupportFragmentManager(),conte!!)

        page_stories_view_pager.setAdapter(adapterViewPager)
        page_stories_view_pager!!.setPageTransformer(false, DefaultTransformer())

    }


    class MyPagerAdapter(fragmentManager: FragmentManager, conte:ContePojo) : FragmentPagerAdapter(fragmentManager) {

        private val mConte=conte

        // Returns total number of pages
        override fun getCount(): Int {
            return mConte.pages!!.size
        }

        // Returns the fragment to display for that page
        override fun getItem(position: Int): Fragment? {
            return PageStoryFragment().newInstance(mConte.pages!![position].texte!!, mConte.pages!![position].image!!)
        }

        // Returns the page title for the top indicator
        override fun getPageTitle(position: Int): CharSequence? {
            return "Page " + position
        }

    }
}
