package com.encoreune.histoire.features.books

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.encoreune.histoire.MyApplication
import com.encoreune.histoire.R
import com.encoreune.histoire.data.BookPojo
import com.encoreune.histoire.features.books.adapters.BooksAdapter
import com.encoreune.histoire.features.settings.SettingsActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.messaging.FirebaseMessaging
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class BooksActivity : AppCompatActivity() {


    private lateinit var adapter: BooksAdapter
    private val RC_SIGN_IN = 123

    private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }

        if(!checkPlayServices()){
            finish()
            Toast.makeText(this,"Google Play services n'est pas installé.",Toast.LENGTH_SHORT).show()
        }

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Ouverture")
        mFirebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, bundle)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName = getString(R.string.default_notification_channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW))
        }

        FirebaseMessaging.getInstance().subscribeToTopic("news")

        sign_in()

        settings.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }
    }

    fun sign_in(){
        val user = FirebaseAuth.getInstance().currentUser
        if(user==null) {
            val providers = Arrays.asList(
                    AuthUI.IdpConfig.GoogleBuilder().build())

            // Create and launch sign-in intent
            loadStories()
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(), RC_SIGN_IN)
        }else loadStories()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode != Activity.RESULT_OK) {
                //Toast.makeText(this,"Vous avez besoin de vous connecter pour continuer.", Toast.LENGTH_SHORT).show()

                val snackbar = Snackbar.make(coordinator_layout, "Vous avez besoin de vous connecter pour continuer.", Snackbar.LENGTH_INDEFINITE)
                snackbar.setAction("Se connecter", SignInListener())
                snackbar.show()
            }else{
                loadStories()
            }
        }
    }

    inner class SignInListener : View.OnClickListener {

        override fun onClick(v: View) {

            sign_in()
        }
    }

    fun loadStories() {
        list_of_books_recyclerView.visibility = View.GONE
        animation_view.visibility = View.VISIBLE


        val database = MyApplication.database
        val myRef = database.getReference("v3/books")

        // Read from the database and retrieve stories for the language defined
        myRef.addValueEventListener(object : ValueEventListener {
            private var list_of_books: ArrayList<BookPojo>? = null

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                list_of_books = ArrayList<BookPojo>()


                for (child in dataSnapshot.children) {

                    val book = child.getValue(BookPojo::class.java)!!

                    list_of_books!!.add(book)
                }


                list_of_books_recyclerView.visibility = View.VISIBLE
                animation_view.visibility = View.GONE


                list_of_books_recyclerView.addOnItemChangedListener { _, adapterPosition ->

                    val tface = Typeface.createFromAsset(this@BooksActivity.assets, "fonts/Royana-Regular.ttf")
                    by.typeface = tface
                    by.text = list_of_books!![adapterPosition].label
                    title_story.typeface=tface
                    title_story.setTextColor(Color.parseColor(list_of_books!![adapterPosition].title_color))
                    by.setTextColor(Color.parseColor(list_of_books!![adapterPosition].title_color))
                    val conte = adapter.getItem(adapterPosition)
                    title_story.text = conte.title

                    val gradient = GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, intArrayOf(Color.parseColor(list_of_books!![adapterPosition].vibrant_color),Color.TRANSPARENT))
                    gradient.setShape(GradientDrawable.RECTANGLE)
                    bottom_gradient_view.background = gradient

                }


                adapter = BooksAdapter(this@BooksActivity, list_of_books!!)
                list_of_books_recyclerView.setAdapter(adapter)
                list_of_books_recyclerView.setItemTransitionTimeMillis(150)
                list_of_books_recyclerView.setItemTransformer(ScaleTransformer.Builder()
                        .setMinScale(0.8f)
                        .build())

            }

            override fun onCancelled(databaseError: DatabaseError) {

                // Failed to read value
                Log.e("databaseError", databaseError.toException().toString())
            }
        })
    }

    fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show()
            } else {
                Log.i("checkPlayServices", "This device is not supported.");
                finish();
            }
            return false
        }
        return true
    }
}
