package com.encoreune.histoire.features.books.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.devs.vectorchildfinder.VectorChildFinder
import com.encoreune.histoire.GlideApp
import com.encoreune.histoire.R
import com.encoreune.histoire.data.BookPojo
import com.encoreune.histoire.features.stories.list.CoverStoriesActivity
import java.util.*

/**
 * Created by Sosthene on 9/9/2017.
 */

internal class BooksAdapter(private val mContext: Activity, private val mValues: ArrayList<BookPojo>) : RecyclerView.Adapter<BooksAdapter.ViewHolder>() {
    private val mBackground: Int

    internal inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mStoryImageView: ImageView
        val mBookImageView: ImageView

        init {
            mStoryImageView = mView.findViewById<View>(R.id.story_image) as ImageView

            mBookImageView = mView.findViewById<View>(R.id.bg_book_vector) as AppCompatImageView
        }

        override fun toString(): String {
            return super.toString()
        }
    }

    init {
        val mTypedValue = TypedValue()
        mContext.theme.resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.book_item_layout, parent, false)
        view.setBackgroundResource(mBackground)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val vector = VectorChildFinder(mContext, R.drawable.vector_drawable_book, holder.mBookImageView)
        val vibrant_color = Color.parseColor(mValues[position].vibrant_color)

        val path1 = vector.findPathByName("bookcolor")
        val path3 = vector.findPathByName("bookcolor1")
        val path4 = vector.findPathByName("bookcolor2")
        val path5 = vector.findPathByName("bookcolor3")
        val path2 = vector.findPathByName("bookcolor_back")

        path1.fillColor = vibrant_color
        path2.fillColor = vibrant_color
        path3.fillColor = vibrant_color
        path4.fillColor = vibrant_color
        path5.fillColor = vibrant_color

        holder.mBookImageView.invalidate()

        GlideApp
                .with(mContext)
                .load(mValues[position].image)
                .into(holder.mStoryImageView)



        holder.mStoryImageView.setOnClickListener { view ->
            val i = Intent(mContext, CoverStoriesActivity::class.java)
            i.putExtra("book", mValues[position])

            val p1 = Pair<View,String>(holder.mStoryImageView as View, "book_image")

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mContext, p1)

            mContext.startActivity(i, options.toBundle())

        }
    }

    fun getItem(position: Int): BookPojo {
        return mValues[position]
    }

    override fun getItemCount(): Int {
        return mValues.size
    }




}