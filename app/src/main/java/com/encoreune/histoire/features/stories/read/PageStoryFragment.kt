package com.encoreune.histoire.features.stories.read


import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.encoreune.histoire.GlideApp
import com.encoreune.histoire.R
import kotlinx.android.synthetic.main.fragment_page_story.*


class PageStoryFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_page_story, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
            val image = arguments!!.getString("image")
            val texte = arguments!!.getString("texte")
            val content : Spanned

            GlideApp.with(this).load(image).into(photo_view)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                content= Html.fromHtml(texte, Html.FROM_HTML_MODE_LEGACY)
            } else {
                content= Html.fromHtml(texte)
            }

            val contentFace = Typeface.createFromAsset(activity!!.assets, "fonts/Royana-Regular.ttf")
            content_story.typeface = contentFace

            if(texte.isEmpty()) {
                content_layout.visibility=View.GONE
                val set = ConstraintSet()

                set.clone(container_layout)
                set.connect(photo_view.id, ConstraintSet.TOP, container_layout.getId(), ConstraintSet.TOP, 0)
                set.connect(photo_view.id, ConstraintSet.BOTTOM, container_layout.getId(), ConstraintSet.BOTTOM, 0)
                set.applyTo(container_layout)

            }
            else content_story.text = content
    }

    fun newInstance(texte: String, image: String): Fragment {

        val args = Bundle()
        args.putString("texte", texte)
        args.putString("image", image)
        val fragment = PageStoryFragment()
        fragment.setArguments(args)
        return fragment
    }


}
