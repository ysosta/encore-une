package com.encoreune.histoire.features.stories.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat
import android.support.v4.util.Pair
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import com.devs.vectorchildfinder.VectorChildFinder
import com.encoreune.histoire.GlideApp
import com.encoreune.histoire.R
import com.encoreune.histoire.data.ContePojo
import com.encoreune.histoire.features.stories.list.CoverStoriesActivity
import com.encoreune.histoire.features.stories.read.StoryReaderActivity
import java.util.*

/**
 * Created by Sosthene on 9/9/2017.
 */

internal class StoriesAdapter(private val mActivity: Activity, private val mValues: ArrayList<ContePojo>,
                              private val isBillingActive: Boolean) : RecyclerView.Adapter<StoriesAdapter.ViewHolder>() {

    private val mBackground: Int

    internal inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        //val mStoryTextView: TextView
        val mStoryImageView: ImageView
        val mStoryBlackOverlayLayout: FrameLayout
        val mStoryLockedImageView: ImageView
        val mBookImageView: ImageView


        init {
            mStoryImageView = mView.findViewById<View>(R.id.story_image) as ImageView
            mStoryLockedImageView = mView.findViewById<View>(R.id.locked_image_story) as ImageView
            mBookImageView = mView.findViewById<View>(R.id.bg_storybook_vector) as AppCompatImageView
            mStoryBlackOverlayLayout = mView.findViewById<View>(R.id.overlay_stories_layout) as FrameLayout
        }

        override fun toString(): String {
            return super.toString()
        }
    }

    init {
        val mTypedValue = TypedValue()
        mActivity.theme.resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.story_item_layout, parent, false)
        view.setBackgroundResource(mBackground)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val vector = VectorChildFinder(mActivity, R.drawable.vector_drawable_book, holder.mBookImageView)
        val vibrant_color = Color.parseColor(mValues[position].vibrant_color)

        val path1 = vector.findPathByName("bookcolor")
        val path3 = vector.findPathByName("bookcolor1")
        val path4 = vector.findPathByName("bookcolor2")
        val path5 = vector.findPathByName("bookcolor3")
        val path2 = vector.findPathByName("bookcolor_back")

        path1.fillColor = vibrant_color
        path2.fillColor = vibrant_color
        path3.fillColor = vibrant_color
        path4.fillColor = vibrant_color
        path5.fillColor = vibrant_color

        holder.mBookImageView.invalidate()

        //val face = Typeface.createFromAsset(mActivity.assets, "fonts/Royana-Regular.ttf")

        //holder.mStoryTextView.setText(mValues[position].title)
        //holder.mStoryTextView.typeface = face

        //if(mValues[position].illustrator.isNullOrEmpty()) holder.mStoryIllustratorTextView.text = ""
        //else holder.mStoryIllustratorTextView.text = "Illustré par ${mValues[position].illustrator}"

        //holder.mStoryIllustratorTextView.typeface = face


        GlideApp
                .with(mActivity)
                .load(mValues[position].image)
                .centerCrop()
                .into(holder.mStoryImageView)

        if (isBillingActive || mValues[position].free!!){
            holder.mStoryLockedImageView.visibility=View.GONE
            holder.mStoryBlackOverlayLayout.foreground = null
        }else{

            holder.mStoryLockedImageView.visibility=View.VISIBLE
            holder.mStoryBlackOverlayLayout.foreground = ColorDrawable(ContextCompat.getColor(mActivity, R.color.black_overlay))
        }

        holder.mStoryImageView.setOnClickListener { view ->

            if (isBillingActive || mValues[position].free!!){
                val i = Intent(mActivity, StoryReaderActivity::class.java)
                i.putExtra("story", mValues[position])

                val p1 = Pair<View,String>(holder.mStoryImageView as View, "book_image")
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity, p1)

                mActivity.startActivity(i, options.toBundle())
                //mActivity.startActivity(i)
            }
            else(mActivity as CoverStoriesActivity).purchaseSubscription()

        }
    }

    fun getItem(position: Int): ContePojo {
        return mValues[position]
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

}