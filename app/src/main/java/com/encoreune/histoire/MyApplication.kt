package com.encoreune.histoire

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatDelegate
import com.encoreune.histoire.utils.PreferencesHelper
import com.google.firebase.FirebaseApp
import com.google.firebase.database.FirebaseDatabase
import io.fabric.sdk.android.Fabric




/**
 * Created by Sosthene on 6/6/2017.
 */


class MyApplication : Application() {
    var preferences: PreferencesHelper? = null
        private set

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        Fabric.with(this)
        instance = this
        initPreferencesHelper()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    //Initialize preferencesHelper
    private fun initPreferencesHelper() {
        preferences = PreferencesHelper(this.applicationContext)
    }

    companion object {
        private var mDatabase: FirebaseDatabase? = null
        @get:Synchronized
        var instance: MyApplication? = null
            private set


        val database: FirebaseDatabase
            get() {
                if (mDatabase == null) {
                    mDatabase = FirebaseDatabase.getInstance()
                    mDatabase!!.setPersistenceEnabled(true)
                }
                return mDatabase as FirebaseDatabase
            }
    }


}

