package com.encoreune.histoire.data

import android.os.Parcel
import android.os.Parcelable

class ContePojo : Parcelable {


    var title: String? = null
    var free: Boolean? = null
    var language: String? = null
    var image: String? = null
    var audio: String? = null
    var content: String? = null
    var illustrator: String? = null
    var title_color: String? = null
    var vibrant_color: String? = null
    var pages: ArrayList<PagePojo>? = null

    constructor() {}

    private constructor(`in`: Parcel) {
        title = `in`.readString()
        free = `in`.readValue(Boolean::class.java.classLoader) as? Boolean
        language = `in`.readString()
        image = `in`.readString()
        audio = `in`.readString()
        content = `in`.readString()
        illustrator = `in`.readString()
        title_color = `in`.readString()
        vibrant_color = `in`.readString()
        pages = `in`.readArrayList(PagePojo::class.java.getClassLoader()) as ArrayList<PagePojo>?
    }

    override fun toString(): String {
        return "ContePojo{" +
                "title='" + title + '\''.toString() +
                "is_free='" + free + '\''.toString() +
                ", language='" + language + '\''.toString() +
                ", image='" + image + '\''.toString() +
                ", audio='" + audio + '\''.toString() +
                ", content='" + content + '\''.toString() +
                ", illustrator='" + illustrator + '\''.toString() +
                ", except_color='" + title_color + '\''.toString() +
                ", vibrant_color='" + vibrant_color + '\''.toString() +
                ", pages='" + pages + '\''.toString() +
                '}'.toString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, i: Int) {
        dest.writeString(title)
        dest.writeValue(free)
        dest.writeString(language)
        dest.writeString(image)
        dest.writeString(audio)
        dest.writeString(content)
        dest.writeString(illustrator)
        dest.writeString(title_color)
        dest.writeString(vibrant_color)
        dest.writeList(pages)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<ContePojo> = object : Parcelable.Creator<ContePojo> {
            override fun createFromParcel(`in`: Parcel): ContePojo {
                return ContePojo(`in`)
            }

            override fun newArray(size: Int): Array<ContePojo?> {
                return arrayOfNulls(size)
            }
        }
    }
}
