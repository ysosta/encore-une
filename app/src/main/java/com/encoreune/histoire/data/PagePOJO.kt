package com.encoreune.histoire.data

import android.os.Parcel
import android.os.Parcelable

class PagePojo : Parcelable {


    var texte: String? = null
    var image: String? = null

    constructor() {}

    private constructor(`in`: Parcel) {
        texte = `in`.readString()
        image = `in`.readString()
    }

    override fun toString(): String {
        return "PagePojo{" +
                "title='" + texte + '\''.toString() +
                ", image='" + image + '\''.toString()+
                '}'.toString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, i: Int) {
        dest.writeString(texte)
        dest.writeString(image)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<PagePojo> = object : Parcelable.Creator<PagePojo> {
            override fun createFromParcel(`in`: Parcel): PagePojo {
                return PagePojo(`in`)
            }

            override fun newArray(size: Int): Array<PagePojo?> {
                return arrayOfNulls(size)
            }
        }
    }
}
