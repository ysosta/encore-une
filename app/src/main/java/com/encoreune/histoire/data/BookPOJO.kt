package com.encoreune.histoire.data

import android.os.Parcel
import android.os.Parcelable



class BookPojo() : Parcelable {

    var title: String? = null
    var image: String? = null
    var label: String? = null
    var title_color: String? = null
    var vibrant_color: String? = null
    var stories: ArrayList<ContePojo>? = null


    private constructor(`in`: Parcel) : this() {
        title = `in`.readString()
        image = `in`.readString()
        label = `in`.readString()
        title_color = `in`.readString()
        vibrant_color = `in`.readString()
        stories = `in`.readArrayList(ContePojo::class.java.getClassLoader()) as ArrayList<ContePojo>?
    }

    override fun toString(): String {
        return "BookPojo{" +
                "title='" + title + '\''.toString() +
                ", image='" + image + '\''.toString() +
                ", label='" + label + '\''.toString() +
                ", title_color='" + title_color + '\''.toString() +
                ", vibrant_color='" + vibrant_color + '\''.toString() +
                ", stories='" + stories + '\''.toString() +
                '}'.toString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, i: Int) {
        dest.writeString(title)
        dest.writeString(image)
        dest.writeString(label)
        dest.writeString(title_color)
        dest.writeString(vibrant_color)
        dest.writeList(stories)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<BookPojo> = object : Parcelable.Creator<BookPojo> {
            override fun createFromParcel(`in`: Parcel): BookPojo {
                return BookPojo(`in`)
            }

            override fun newArray(size: Int): Array<BookPojo?> {
                return arrayOfNulls(size)
            }
        }
    }



}
