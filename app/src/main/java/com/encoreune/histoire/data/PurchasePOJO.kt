package com.encoreune.histoire.data

class PurchasePojo() {

    var orderId: String? = null
    var packageName: String? = null
    var productId: String? = null
    var purchaseTime: String? = null
    var purchaseToken: String? = null
    var autoRenewing: Boolean? = null
    var expiryTimeMillis: String? = null
    var isActive: Boolean? = null


}
