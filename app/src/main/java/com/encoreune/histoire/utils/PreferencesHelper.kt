package com.encoreune.histoire.utils

import android.content.Context
import android.content.SharedPreferences


/**
 * Reads and saves data in the SharedPreferences
 */
class PreferencesHelper(context: Context) {
    // Shared Preferences
    private val mPref1: SharedPreferences

    // Editor for Shared preferences
    private val mEditorSession: SharedPreferences.Editor

    var keyFirstTime: Boolean?
        get() = mPref1.getBoolean(KEY_FIRST_TIME, true)
        set(isFirst) {
            mEditorSession.putBoolean(KEY_FIRST_TIME, isFirst!!)
            mEditorSession.commit()
        }

    var keySubscribeStoriesChannelNotif: Boolean?
        get() = mPref1.getBoolean(KEY_SUBSCRIBE_STORIES_CHANNEL_NOTIF, false)
        set(value) {
            mEditorSession.putBoolean(KEY_SUBSCRIBE_STORIES_CHANNEL_NOTIF, value!!)
            mEditorSession.commit()
        }

    var keyLanguage: String
        get() = mPref1.getString(KEY_LANGUAGE, "")
        set(language) {
            mEditorSession.putString(KEY_LANGUAGE, language)
            mEditorSession.commit()
        }

    var keyIsBillingActive: Boolean
        get() = mPref1.getBoolean(KEY_IS_BILLING_ACTIVE, false)
        set(state) {
            mEditorSession.putBoolean(KEY_IS_BILLING_ACTIVE, state)
            mEditorSession.commit()
        }

    var keyEndingTimeBilling: Long
        get() = mPref1.getLong(KEY_ENDING_TIME_BILLING, 0)
        set(time) {
            mEditorSession.putLong(KEY_ENDING_TIME_BILLING, time)
            mEditorSession.commit()
        }

    init {
        mPref1 = context.getSharedPreferences(PREF_SESSION_NAME, Context.MODE_PRIVATE)
        mEditorSession = mPref1.edit()
    }

    companion object {

        // Sharedpref file name
        private val PREF_SESSION_NAME = "0210wksasamndjas02_)*(@&^"

        private val KEY_FIRST_TIME = "first_time"
        private val KEY_IS_BILLING_ACTIVE = "is_billing_active"
        private val KEY_ENDING_TIME_BILLING = "ending_time_billing"
        private val KEY_LANGUAGE = "language"
        private val KEY_SUBSCRIBE_STORIES_CHANNEL_NOTIF = "msubscribe_stories_channel"
    }

}
